# -*- coding: utf-8 -*-
""" Module contains common evaluation functions

Special for evaluating LTE system and some other common evaluation methods
"""
import numpy as np
import csv, os, re, time, platform
import mmap
# import for unpacking timespec struct
from struct import unpack
from datetime import datetime

__author__ = "Igor Kim"
__credits__ = ["Igor Kim"]
__maintainer__ = "Igor Kim"
__email__ = "igor.skh@gmail.com"
__status__ = "Development"
__date__ = "01/2018"
__license__ = ""

def unpackRiv(RIV, N_RB_UL):
    """Unpack RBstart and Lcrb from RIV value according to 3GPP TS 36.213-8.1.1
    
    Arguments:
        RIV {integer} -- given RIV value
        N_RB_UL {integer} -- number of resource blocks for uplink
    
    Returns:
        (integer, integer, integer) -- position of the start RB, length of RBs group, position of the last RB
    """
    # define equations from 3GPP TS 36.213-8.1.1
    eq1 = lambda x, y: N_RB_UL * (x - 1) + y
    eq2 = lambda x, y: N_RB_UL * (N_RB_UL - x + 1) + (N_RB_UL - 1 - y)
    # init result values
    RBstart = -1
    Lcrb = 0
    # i - RBstart, j - Lcrb
    # try all options, solving two variables from one equation
    for i in range(N_RB_UL):
        for j in range(1,N_RB_UL+1-i):
            if eq1(j,i)==RIV or eq2(j,i)==RIV:
                RBstart, Lcrb = i, j
    # calculate the last RB position
    RBStop = RBstart + Lcrb - 1
    return RBstart, Lcrb, RBStop

def findTimestamp(s, fileDatetime=""):
    """Find a timestamp in a string
    
    Arguments:
        s {string} -- string where to search
    
    Returns:
        (float, string) -- output timestamp and string without it
    """
    possibleTs = [r"([0-9-]{10})\.([0-9]{6,9})", r"([0-9-]{10}T[0-9:]{8})\.([0-9]{6,9})", r"([0-9:]{8})\.([0-9]{6,9})"]
    foundTsId = -1
    ts = None
    for reeId in range(len(possibleTs)):
        m = re.search(possibleTs[reeId], s)
        if not m is None:
            foundTsId = reeId
            break
    if foundTsId > -1:
        s = s[s.find(m.group(0))+len(m.group(0))+1:]
        if foundTsId == 0:
            sec = m.group(1)
        elif foundTsId in [1,2]:
            secStr = m.group(1)
            if foundTsId == 2:
                local_time = time.localtime(fileDatetime)
                secStr = time.strftime("%Y-%m-%dT", local_time) + secStr
                format = "%H:%M:%S"
            format = "%Y-%m-%dT%H:%M:%S"
            sec = int(time.mktime(datetime.strptime(secStr, format).timetuple()))
        ts = str(sec) + "." + m.group(2)
    return ts, s

def getCurrentUtc():
    """Get current UTC time    
    Returns:
        float -- current UTC timestamp
    """
    dts = datetime.utcnow()
    epochtime = time.mktime(dts.timetuple()) + dts.microsecond/1e6
    return epochtime

def rmFile(filepath):
    """Try to remove file or do nothing
    
    Arguments:
        filepath {[string]} -- path to a file
    Returns:
        bool -- True if file was found and removed, False otherwise 
    """
    try:
        os.remove(filepath)
        return True
    except OSError:
        return False


def getFileDatetime(path_to_file):
    """
    Try to get the date that a file was created, falling back to when it was
    last modified if that isn't possible.
    See http://stackoverflow.com/a/39501288/1709587 for explanation.
    """
    if platform.system() == 'Windows':
        return os.path.getctime(path_to_file)
    else:
        stat = os.stat(path_to_file)
        try:
            return stat.st_birthtime
        except AttributeError:
            # We're probably on Linux. No easy way to get creation dates here,
            # so we'll settle for when its content was last modified.
            return stat.st_mtime

def parseLogLineToCSV(line, keyword, fileDatetime, header, delimiter="|", kvDelimiter=" "):
    line = str(line)
    result_row = {}
    if line.find(keyword) > -1:
        ts, _ = findTimestamp(line, fileDatetime)
        if not ts is None:
            result_row['timestamp'] = ts
            if not 'timestamp' in header:
                header.append('timestamp')
        r = line.strip().split(delimiter)[1:]
        for v in r:
            v_split = v.strip().split(kvDelimiter)
            if not v_split[0] in header:
                header.append(v_split[0])
            result_row[v_split[0]] = int(v_split[1])
    return result_row

def parseLogToCSV(filepath, keyword, initialHeader = [], delimiter="|", kvDelimiter=" ", filepathCSV=None):
    """Convert text log file to a CSV one
    
    Arguments:
        filepath {string} -- path to a file
        columns {list} -- list of columns
    """
    if filepathCSV is None:
        filepathCSV = filepath + ".csv"
    fileDatetime = getFileDatetime(filepath)    
    #read the file to variable content
    with open(filepath, "r") as f:
        content = f.readlines()
    result = [] 
    header = []
    for el in initialHeader:
        header.append(el)
    for line in content:
        result_row = parseLogLineToCSV(line, keyword, fileDatetime, 
            header, delimiter, kvDelimiter)
        if len(result_row) > 0:
            result.append(result_row)
    with open(filepathCSV, "w") as f:
        writer = csv.writer(f, quoting=csv.QUOTE_MINIMAL)
        writer.writerow(header)
        for el in result:
            app = []
            for l in header:
                if l not in el:
                    app.append(0)
                else:
                    app.append(el[l])
            writer.writerow(app)

def rivToBitmapArray(RIV, N_RB_UL):
    """Unpack RIV and convert to list showing position of allocated RBs according to given RIV
    
    Arguments:
        RIV {integer} -- given RIV value
        N_RB_UL {integer} -- number of resource blocks for uplink
    """
    # unpack RIV
    RBstart, _, RBStop = unpackRiv(RIV, N_RB_UL)
    bitMap = [int(i >= RBstart and i <= RBStop) for i in range(N_RB_UL)]
    return  bitMap

def appendToCSV(values, csvFile="test.csv"):
    """Append list entries to the CSV file
    
    Arguments:
        values {list} -- values to add
    """
    with open(csvFile, 'a') as csvfile:
        spamwriter = csv.writer(csvfile, quoting=csv.QUOTE_MINIMAL)
        spamwriter.writerow(values)

def getCpuFreqMHz():
    """Get CPU frequency in MHz, works only on Linux
    """
    h = os.popen("cat /proc/cpuinfo | grep MHz")
    return float(h.readlines()[0].split(":")[1].strip())

def formatTimestampToString(ts=None, fmt= '%Y-%m-%d %H:%M:%S.%f'):
    """Format timestamp to string

    Arguments:
        ts {float} -- timestamp value, if None - current
        fmt {string} -- format for the output

    Returns:
        string -- formatted string or empty if exception occured
    """
    dt = datetime.now() if ts is None else datetime.fromtimestamp(ts)
    try:
        return dt.strftime(fmt)
    except:
        return ""

def unpackTimespec(byteArr):
    """Unpacks C struct timespec from bytes defined as (long long, long)"""
    try:
        tStruct = unpack('ql', byteArr)
        timestampObj = (10e8 * int(tStruct[0]) + tStruct[1])/10e8
        return timestampObj
    except:
        return 0 

def convertIntToBinIntArray(sbinInt, length):
    """Convert integer var to a list of bin of defined length
    
    Arguments:
        sbinInt {integer} -- integer which has to be converted
        length {integer} -- final length of a list

    Returns:
        list -- list contains int converted to bin
    """
    inStr = str(bin(sbinInt))[2:]
    inStr = "0"*(length - len(inStr)) + inStr
    binArr = [int(inStr[i]) for i in range(length)] 
    return binArr

def calcCdfOrCcdf(values, isCcdf=False):
    """Calculate CDF or CCDF for a given list
    
    Arguments:
        values {list} -- values to calculate CDF/CCDF
    
    Keyword Arguments:
        isCcdf {boolean} -- calculate CCDF if true otherwise CDF (default: {False})
    
    Returns:
        (list, list) -- x axis values (parameter), y axis values (CDF)
    """
    # convert to a numpy array
    values = np.array(values)
    # sort values
    values = np.sort(values)
    # normalize values to the sum of all
    valuesNorm = values/ sum(values)
    # calc the cumulative sum
    cY = 1 - np.cumsum(valuesNorm) if isCcdf else np.cumsum(valuesNorm)
    return values, cY

def calcDiffValues(values, sortBefore = False):
    """Calcaulte differences between each two values
    
    Arguments:
        values {list} -- list of values to calculate differences
    
    Keyword Arguments:
        sortBefore {boolean} -- sort the input list before calculation (default: {False})

    Returns: 
        list -- differences between each two values
    """
    # convert to ms
    values = np.sort(np.array(values)) if sortBefore else  np.array(values)
    diffs  = values[1:] - values[:-1]
    return diffs

def tryMapValue(val, mappingFunc):
    """Tries to map a value with a given function
    
    Arguments:
        val {object} -- any given value is to be mapped
        mappingFunc {function} -- mapping method is to be used
    
    Returns:
        object -- None if couldn't map otherwise mapped value
    """
    try:
        return mappingFunc(val)
    except ValueError:
        return None

def linesCount(filename):
    """Count number of lines in the file
    
    Arguments:
        filename {string} -- filepath
    
    Returns:
        integer -- number of lines in the file
    """
    non_blank_count = 0

    with open(filename) as infp:
        for line in infp:
            if line.strip():
                non_blank_count += 1
    return non_blank_count    

def readCSVFile(filename, nValues=50000, header=[], defaultMapFunc=None, mappingFuncs=[]):
    """Read a CSV file, try to auto-detect a header
    
    Arguments:
        filename {string} -- filepath to the CSV file
    
    Keyword Arguments:
        nValues {integer} -- number of values to be read (default: {50000})
    """
    # function tries to determine a header in a first line by using mapping 
    # or inconsistence of a given header to the file row
    # TODO: find a smarter way to determine a header, maybe compare first several lines and determine how different the first line is
    
    # init result list
    result = []
    with open(filename, 'r') as csvfile:
        # initialize reader
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
        # initialize values
        i = 0
        tmpHeader = []
        isHeader = False
        for row in spamreader:
            # if reached the limit
            if i == nValues:
                break
            # initialize values for each row
            resultRow = {}
            tmpRow = {}
            for j in range(len(row)):
                # use map func from the list of default if not defined
                mapFunc = mappingFuncs[j] if j < len(mappingFuncs) else defaultMapFunc
                # try to map value with a function
                mappedValue = tryMapValue(row[j], mapFunc) if not mapFunc is None else row[j]
                # if the first line checking for the header
                if i == 0:
                    # if the mapping was unsuccesfull, assume this is a header
                    if mappedValue is None:
                        isHeader = True
                    # otherwise store as a normal value
                    elif not isHeader and j < len(header):
                        tmpRow[header[j]] = mappedValue
                    # store to a temp header anyway as we don't know the result for the next values
                    tmpHeader.append(row[j])
                else:
                    if mappedValue is None:
                        raise ValueError("Mapping failed, probably wrong CSV format")
                    resultRow[header[j]] = mappedValue
            # if header found on the first line
            if i == 0 and isHeader:
                header = tmpHeader
            # if first line but the header
            elif i == 0:
                resultRow = tmpRow

            # if not a first line or not a header
            if i != 0 or not isHeader:
                result.append(resultRow)
            i += 1
    return result, header